Array.prototype.toString = function() {
    return this.join(", ").trim();
};

function Select() {
    
    this.distinct = false;
    this.columns = "?";
    this.tables = "?";
    
    this.join = null;
    this.where = null;
    this.groupBy = null;
    this.having = null;
    this.orderBy = null;
    
    this.toString = function() {
        return "SELECT" + (this.distinct ? " DISTINCT " : " ") + this.columns.toString() + " FROM " + this.tables.toString() + 
                (this.join !== null ? " " + this.join.toString() : "") + 
                (this.where !== null ? " " + this.where.toString() : "") +
                (this.groupBy !== null ? " " + this.groupBy.toString() : "") +
                (this.having !== null ? " " + this.having.toString() : "") +
                (this.orderBy !== null ? " " + this.orderBy.toString() : "");
    };
    
}

function Join() {
    
    this.type = "INNER";
    this.table = "?";
    this.column = "?";
    this.value = "?";
    
    this.toString = function() {
        return this.type + " JOIN " + this.table + " ON " + this.column + " = " + this.value;
    };
    
}

function Where() {
    
    this.column = "?";
    this.operator = "=";
    this.value = "?";
    
    this.toString = function() {
        return "WHERE " + this.column + " " + this.operator + " " + this.value;
    };
    
}

function GroupBy() {
    
    this.columns = "?";
    
    this.toString = function() {
        return "GROUP BY " + this.columns.toString();
    };
    
}

function Having() {
    
    this.func = "?";
    this.operator = "=";
    this.value = "?";
    
    this.toString = function() {
        return "HAVING " + this.func + " " + this.operator + " " + this.value;
    };
    
}

function OrderBy() {
    
    this.column = "?";
    this.ascending = true;
    
    this.toString = function() {
        return "ORDER BY " + this.column + (this.ascending ? " ASC" : " DESC");
    };
    
}

statement = new Select();