$.fn.selectable = function() {
    var self = this;
    var selectionBox = $("<div>").addClass("selection-box");
    var removeBox = function() {
        self.off("mousemove");
        selectionBox.remove();
    };
    self.mousedown(function(e) {
        var position = self.position();
        var x = position.left;
        var y = position.top;
        var clickX = e.pageX - x;
        var clickY = e.pageY - y;
        selectionBox.css({
            height: 0,
            left: clickX,
            top: clickY,
            width: 0
        }).appendTo(self);
        self.mousemove(function(e) {
            var moveX = e.pageX - x;
            var moveY = e.pageY - y;
            var height = Math.abs(moveY - clickY);
            var width = Math.abs(moveX - clickX);
            selectionBox.css({
                height: height,
                left: moveX < clickX ? clickX - width : clickX,
                top: moveY < clickY ? clickY - height : clickY,
                width: width
            });
        }).mouseleave(removeBox).mouseup(removeBox);
    });
};