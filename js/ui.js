var search = function () {
    var string = $(this).val().trim().toUpperCase();
    $("#blocks .block").each(function () {
        var block = $(this);
        if (block.text().indexOf(string) > -1) {
            block.show();
        }
        else {
            block.hide();
        }
    });
};

$(document).ready(function() {
    //Search Functionality
    $("input[type=text]").on("input", search);
    //Drag and Drop System
    $("#center").selectable();
    var body = $(document.body);
    var center = $("#center");
    var dragged = null;
    var drag = function(e) {
        var block = $(this);
        dragged = block.clone();
        if (block.parent().is("#center")) {
            block.remove();
        }
        body.append(dragged);
        dragged.addClass("dragged").css({
            left: e.pageX - dragged.outerWidth() / 2,
            top: e.pageY - dragged.outerHeight() / 2
        });
    };
    $(".block").on("mousedown", drag);
    body.on("mousemove", function(e) {
        if (dragged !== null) {
            var x = e.pageX;
            var y = e.pageY;
            dragged.css({
                left: x - dragged.outerWidth() / 2,
                top: y - dragged.outerHeight() / 2
            });
            var offset = center.offset();
            var left = offset.left;
            var top = offset.top;
            if (x > left && x < left + center.outerWidth() && y > top && y < top + center.outerHeight()) {
                dragged.css({
                    opacity: 1
                });
            }
            else {
                dragged.css({
                    opacity: 0.5
                });
            }
        }
    });
    body.on("mouseup", function(e) {
        if (dragged !== null) {
            var x = e.pageX;
            var y = e.pageY;
            var offset = center.offset();
            var left = offset.left;
            var top = offset.top;
            if (x > left && x < left + center.outerWidth() && y > top && y < top + center.outerHeight()) {
                var block = dragged.clone().removeClass("dragged");
                var blockOffset = dragged.offset();
                block.css({
                    left: blockOffset.left - left,
                    position: "absolute",
                    top: blockOffset.top - top
                });
                center.append(block);
                addInputFields(block);
            }
            dragged.remove();
            dragged = null;
        }
    });
});

var addInputFields = function(block) {
    switch (block.text()) {
        case "SELECT":
            addInputFieldsSelect(block);
            break;
        case "JOIN":
            addInputFieldsJoin(block);
            break;
        case "WHERE":
            addInputFieldsWhere(block);
            break;
        case "GROUP BY":
            addInputFieldsGroupBy(block);
            break;
        case "HAVING":
            addInputFieldsHaving(block);
            break;
        default:
            addInputFieldsOrderBy(block);
    }
};

var addInputFieldsSelect = function(block) {
    var distinct = $("<input>").attr("type", "checkbox").change(function(event) {
        statement.distinct = $(this).val();
        $("#south").text(statement.toString());
    });
    var columns = $("<input>").attr("type", "text").on("input", function(event) {
        statement.columns = $(this).val();
        $("#south").text(statement.toString());
    });
    var tables = $("<input>").attr("type", "text").on("input", function(event) {
        statement.tables = $(this).val();
        $("#south").text(statement.toString());
    });
    block.append($("<p>").text("Distinct"));
    block.append(distinct);
    block.append($("<p>").text("Columns"));
    block.append(columns);
    block.append($("<p>").text("Tables"));
    block.append(tables);
    $("#south").text(statement.toString());
};

var addInputFieldsJoin = function(block) {
    statement.join = new Join();
    var table = $("<input>").attr("type", "text").on("input", function(event) {
        statement.join.table = $(this).val();
        $("#south").text(statement.toString());
    });
    var column = $("<input>").attr("type", "text").on("input", function(event) {
        statement.join.column = $(this).val();
        $("#south").text(statement.toString());
    });
    var value = $("<input>").attr("type", "text").on("input", function(event) {
        statement.join.value = $(this).val();
        $("#south").text(statement.toString());
    });
    block.append($("<p>").text("Table"));
    block.append(table);
    block.append($("<p>").text("Column"));
    block.append(column);
    block.append($("<p>").text("Value"));
    block.append(value);
    $("#south").text(statement.toString());
};