package mysqlvisualbuilder;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JPanel;
/**
 *
 * @author Nelnel33
 */
public class Test extends JPanel implements MouseMotionListener, MouseListener, ActionListener{
    
    public ArrayList<Block> blocks;
    
    public Test(){
        blocks = new ArrayList();
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        
        addBlocks();
        setPreferredSize(new java.awt.Dimension(500,500));
    }
    
    private void addBlocks(){
        blocks.add(new UnaryBlock(new Point(10,10), new Dimension(30,30)));        
        blocks.add(new UnaryBlock(new Point(80,10), new Dimension(30,30)));
        blocks.add(new UnaryBlock(new Point(10,80), new Dimension(30,30)));
        blocks.add(new UnaryBlock(new Point(120,80), new Dimension(30,30)));
        blocks.add(new UnaryBlock(new Point(10,300), new Dimension(30,30)));

    }
    
    @Override
    public void paintComponent(Graphics g){
        Graphics2D gd = (Graphics2D)g;
        
        Block.allRender(gd, blocks);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        Block.allMousePressed(e, blocks);
        repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        Block.allMouseReleased(e, blocks);
        repaint();
    }
    
    @Override
    public void mouseMoved(MouseEvent e) {
        Block.allMouseMoved(e, blocks);
        repaint();
    }
    
    @Override
    public void mouseDragged(MouseEvent e) {
        Block.allMouseMoved(e, blocks);
        repaint();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {}

    @Override
    public void mouseClicked(MouseEvent e) {}
    
    @Override
    public void mouseExited(MouseEvent e) {}
    
    @Override
    public void mouseEntered(MouseEvent e) {}
    
    public static void main(String[] args){
        JFrame frame = new JFrame("Test");
        Test panel = new Test();
        
        frame.add(panel);
        
        frame.setResizable(false);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);    
        frame.pack();
        frame.setLocationRelativeTo(null);
    }
    
}
