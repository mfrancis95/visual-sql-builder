/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mysqlvisualbuilder;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author Nelnel33
 */
public class UnaryBlock extends Block{
    
    private Block child;
    
    public UnaryBlock(Point location, Dimension size) {
        super(location, size);
    }

    public Block getChild() {
        return child;
    }

    public void setChild(Block child) {
        this.child = child;
    }
    
    @Override
    public Block getTail() {
        if (this.child == null) {
            return this;
        } 
        else {
            return this.child.getTail();
        }
    }

    @Override
    public void render(Graphics2D g) {
        if(isPreview()){
            g.setColor(Color.red);
            g.draw(new Rectangle2D.Double(getLocation().x,getLocation().y,getSize().width,getSize().height));
        }
        else{
            g.setColor(Color.blue);
            g.draw(new Rectangle2D.Double(getLocation().x,getLocation().y,getSize().width,getSize().height));
        }
        if(child != null){
            child.render(g);
        }
    }

    @Override
    public void onDropOnBlock(Block other) {        
        if(child != null){
            child.onDropOnBlock(other);
        }
        else{
            child = other;
            child.setParent(this);
            
            child.resizeBlocksFromTailToHead(GROWTH);
        }
    }
    
    @Override
    public void previewWith(Block other) {
        
    }
    
    @Override
    public void resizeBlocksFromTailToHead(int amount) {
        if(this.child == null && this.getParent() != null){
            this.getParent().resizeBlocksFromTailToHead(amount);//does nothing, proceeds to next recursive call.
        }
        else if(this.getParent() == null){
            this.setSize(new Dimension(child.getSize().width+amount, child.getSize().height+amount));
            
            this.repositionBlocksFromHeadToTail(amount);
        }
        else{
            this.setSize(new Dimension(child.getSize().width+amount, child.getSize().height+amount));
            this.getParent().resizeBlocksFromTailToHead(amount);
        }
    }
    
    @Override
    public void repositionBlocksFromHeadToTail(int amount) {
        if(this.getParent() == null && this.child != null){
            this.child.repositionBlocksFromHeadToTail(amount);//does nothing, proceeds to next recursive call.
        }
        else if(this.child == null && this.getParent() == null){
            
        }
        else if(this.child == null){
            this.setLocation(new Point(this.getParent().getLocation().x+amount, this.getParent().getLocation().y+amount));
        }
        else{
            this.setLocation(new Point(this.getParent().getLocation().x+amount, this.getParent().getLocation().y+amount));            
            this.child.repositionBlocksFromHeadToTail(amount);
        }
    }
    
    @Override
    public Block removeBlock(MouseEvent me) {
        Block tail = this.getTail();
        while(tail != null){
            if(tail.isWithinBlock(me)){
                return tail;
            }            
            else{
                tail = tail.getParent();
            }
        }
        
        return tail;
    }
    
    @Override
    public void print() {}  
}
