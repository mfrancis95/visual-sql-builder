package mysqlvisualbuilder;


import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 *
 * @author Nelnel33
 * 
 */
public abstract class Block {
    /**
     * Location of Block.
     */
    private Point location;
    
    /**
     * Size of Block.
     */
    private Dimension size;
    
    /**
     * True if you click and held the block.
     */
    private boolean canBlockMove;
    
    /**
     * Difference between where the mouse has been clicked and dragged and the location of the Block.
     */
    private Point difference;
    
    /**
     * Will be an SQL Statement type when implemented in Javascript.
     */
    private String sqlStatement;
    
    /**
     * Parent of this block.
     */
    private Block parent;
    
    /**
     * Should the block render a preview or be final.
     * If true and it is a preview than no child will be assign
     * Else a child will be assigned and whatever is rendered will be what is encapsulated in Block.
     */
    private boolean isPreview;
    
    /**
     * Amount of pixels the Block will grow each time a child/children are added.
     */
    public static final int GROWTH = 10;
    
    /**
     * Amount of pixels the Block will shrink each time a child/children are removed;.
     */
    public static final int SHRINK = GROWTH*-1;
    
    public Block(Point location, Dimension size){
        this.location = location;
        this.size = size;
    }

    public Point getLocation() {
        return location;
    }

    public void setLocation(Point location) {
        this.location = location;
    }
    
    public Dimension getSize() {
        return size;
    }

    public void setSize(Dimension size) {
        this.size = size;
    }

    public Block getParent() {
        return parent;
    }

    public void setParent(Block parent) {
        this.parent = parent;
    }

    public String getSqlStatement() {
        return sqlStatement;
    }

    public void setSqlStatement(String sqlStatement) {
        this.sqlStatement = sqlStatement;
    }    
    
    public Block getHead(){
        if(this.parent == null){
            return this;
        }
        else{
            return this.parent.getHead();
        }
    }  
    
    public abstract Block getTail();

    public boolean isPreview() {
        return isPreview;
    }

    public void setIsPreview(boolean isPreview) {
        this.isPreview = isPreview;
    }

    public boolean collides(Block other){
        Point close = new Point(location.x,location.y);
        Point far = new Point(location.x+size.width, location.y+size.height);
        Point oclose = new Point(other.location.x, other.location.y);
        Point ofar = new Point(other.location.x+other.size.width, other.location.y+other.size.height);
        
        return !(close.y>ofar.y || far.y<oclose.y || close.x>ofar.x || far.x<oclose.x);
    }
    
    public boolean isWithinBlock(MouseEvent me){
        return isWithinBlock(me.getX(),me.getY());
    }
    
    public boolean isWithinBlock(int x, int y){
        //System.out.println("x="+x+" y="+y+" location.x="+location.x+" location.y="+location.y+" "
                //+ "location.x+size.width="+(location.x+size.width)+" location.y+size.height"+(location.y+size.height));
        
        return x>=location.x && x<=(location.x+size.width) && y>=location.y && y<=(location.y+size.height);
        
    }
    
    public void mouseMoved(MouseEvent me, ArrayList<Block> blocks){
        if(canBlockMove){            
            Block cb =  this.collidedBlock(blocks);
            this.isPreview = true;
            if(cb!=null){
                cb.isPreview = true;
            }
            this.location.x = me.getX()-difference.x;
            this.location.y = me.getY()-difference.y;
            this.repositionBlocksFromHeadToTail(GROWTH);
            //System.out.println("moved");
        }
        update(me, blocks);
    }
    
    public void mousePressed(MouseEvent me, ArrayList<Block> blocks){
        if(me.getButton() == MouseEvent.BUTTON1){
            if (isWithinBlock(me)) {
                //System.out.println("isWithinBlock? "+isWithinBlock(me));
                canBlockMove = true;
                int mx = me.getX();
                int my = me.getY();
                double x = this.location.x;
                double y = this.location.y;
                double dfx = mx - x;
                double dfy = my - y;

                difference = new Point(dfx, dfy);

                //Sends the block that is pressed on to the last index so it is rendered in front(or last).
                //Creates a fake "focus".
                //Also creates precedence, in order to tell which block is being dragged and which one is non-moving.
                for (int i = 0; i < blocks.size(); i++) {
                    if (blocks.get(i).equals(this)) {
                        if (i == blocks.size() - 1) {
                        } 
                        else {
                            blocks.add(blocks.remove(i));
                        }
                    }
                }
            }  
            System.out.println("pressed");
        }
    }
    
    
    public void mouseReleased(MouseEvent me, ArrayList<Block> blocks){
        if(me.getButton() == MouseEvent.BUTTON1){
            setAllBlocksIsPreviewToFalse(blocks);
            canBlockMove = false;
            difference = null;
            update(me, blocks);
            System.out.println("released");
        }
        if(me.getButton() == MouseEvent.BUTTON3){
            removeBlockFromCanvas(me,blocks);
            System.out.println("hello");
        }
    }    
    
    public static Block removeBlockFromCanvas(MouseEvent me, ArrayList<Block> blocks){
        for(int i=0;i<blocks.size();i++){
            if(blocks.get(i).isWithinBlock(me)){
                Block toRemove = blocks.get(i).removeBlock(me);
                if(toRemove.equals(blocks.get(i))){
                    return blocks.remove(i);
                }
                else{
                    Block toRemoveParent = toRemove.getParent();
                    if(toRemoveParent instanceof UnaryBlock){
                        UnaryBlock ur = (UnaryBlock)toRemoveParent;
                        Block child = ur.getChild();
                        ur.setChild(null);
                        return child;
                    }
                    else if(toRemoveParent instanceof BinaryBlock){
                        BinaryBlock br = (BinaryBlock)toRemoveParent;
                        return null;//TODO implement for binaryblocks!
                    }
                    else{                       
                        System.err.println("Not a valid block!");
                        return null;
                    }
                }
            }
        }
        return null;
    }    
    
    /**
     * Checks which block this block is colliding with.
     * Returns null if collides with no block.
     * @param blocks
     * @return 
     */
    public Block collidedBlock(ArrayList<Block> blocks){
        for(int i=0;i<blocks.size();i++){
            if(blocks.get(i).equals(this)){}
            else{
                if(blocks.get(i).collides(this)){
                    return blocks.get(i);
                }
            }
        }
        return null;
    }
    
    public static void setAllBlocksIsPreviewToFalse(ArrayList<Block> blocks){
        for(int i=0;i<blocks.size();i++){
            blocks.get(i).isPreview = false;
        }
    }

    public static void allMouseMoved(MouseEvent me, ArrayList<Block> blocks){
        for(int i=0;i<blocks.size();i++){
            blocks.get(i).mouseMoved(me, blocks);
        }
    }
    
    public static void allMousePressed(MouseEvent me, ArrayList<Block> blocks){
        for(int i=0;i<blocks.size();i++){
            blocks.get(i).mousePressed(me, blocks);
        }
    }
    
    public static void allMouseReleased(MouseEvent me, ArrayList<Block> blocks){
        for(int i=0;i<blocks.size();i++){
            blocks.get(i).mouseReleased(me, blocks);
        }
    }
    
    public static void allRender(Graphics2D g, ArrayList<Block> blocks){
        for(int i=0;i<blocks.size();i++){
            blocks.get(i).render(g);
        }
    }
    
    /**
     * Checks from 0 to size-2(last index[size-1] reserved for dragged block) 
     * to see which block is collide with last index. 
     * @param me
     * @param blocks 
     */
    public void update(MouseEvent me, ArrayList<Block> blocks){
        for(int i=0;i<blocks.size()-1;i++){
            if(this.collides(blocks.get(blocks.size()-1)) && !this.equals(blocks.get(blocks.size()-1))){                
                if(!isPreview && !blocks.get(blocks.size()-1).isPreview){
                    this.onDropOnBlock(blocks.get(blocks.size()-1));
                    blocks.remove(blocks.size()-1);
                }
            }

        }
        
    }    
    
    public boolean equals(Block o){
        if(sqlStatement == null || o.sqlStatement == null){
            return location.equals(o.location) && size.equals(o.size);
        }
        else{
            return location.equals(o.location) && size.equals(o.size) && sqlStatement.equals(o.sqlStatement);
        }
    }
    
    /**
     * What is done when other block is on this block but hasn't been dropped yet.
     * @param other 
     */
    public abstract void previewWith(Block other);
    
    /**
     * Repositions each of the blocks.
     * Traverses the list in forward order.
     */
    public abstract void repositionBlocksFromHeadToTail(int amount);
    
    /**
     * Resizes each of the blocks by a particular amount.
     * Will traverse the list in reverse.
     * @param amount 
     */
    public abstract void resizeBlocksFromTailToHead(int amount);
    
    /**
     * What is done when other block has been dropped onto this one.
     * @param other 
     */
    public abstract void onDropOnBlock(Block other);      
    
    /**
     * Returns the block that is to be removed.
     * @param me
     * @return 
     */
    public abstract Block removeBlock(MouseEvent me);
    
    /**
     * Renders the block.
     * @param g 
     */
    public abstract void render(Graphics2D g);
    
    /**
     * Prints the SQL state from this particular node.
     */
    public abstract void print();
}
