/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mysqlvisualbuilder;

/**
 *
 * @author Nelnel33
 */
public class Dimension {
    public double width;
    public double height;

    public Dimension(double width, double height) {
        this.width = width;
        this.height = height;
    }

    public boolean equals(Dimension o) {
        return width == o.width && height == o.height;
    }
}
