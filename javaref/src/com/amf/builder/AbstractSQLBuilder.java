package com.amf.builder;

public abstract class AbstractSQLBuilder {
    
    protected final StringBuilder statement;
    
    protected AbstractSQLBuilder(StringBuilder statement) {
        this.statement = statement;
    }
    
    public String toString() {
        return statement.toString();
    }
    
}