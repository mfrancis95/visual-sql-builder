package com.amf.builder;

public enum Order {
    
    ASCENDING("ASC"), DESCENDING("DESC");
    
    private final String keyword;
    
    Order(String keyword) {
        this.keyword = keyword;
    }
    
    public String toString() {
        return keyword;
    }
    
}