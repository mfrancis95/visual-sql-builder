package com.amf.builder;

import com.amf.builder.select.SelectBuilder;

public class SQLBuilder {
    
    private SQLBuilder() {}
    
    public static SelectBuilder select(boolean distinct, String columns, String tables) {
        return new SelectBuilder(distinct, columns, tables);
    }
    
}