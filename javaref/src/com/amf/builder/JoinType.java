package com.amf.builder;

public enum JoinType {
    
    INNER_JOIN("INNER JOIN"), LEFT_OUTER_JOIN("LEFT OUTER JOIN"), RIGHT_OUTER_JOIN("RIGHT OUTER JOIN");
    
    private final String keywords;
    
    JoinType(String keywords) {
        this.keywords = keywords;
    }
    
    public String toString() {
        return keywords;
    }
    
}