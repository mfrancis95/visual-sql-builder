package com.amf.builder.select;

import com.amf.builder.AbstractSQLBuilder;
import com.amf.builder.Operator;

public class GroupByBuilder extends AbstractSQLBuilder implements Union {

    GroupByBuilder(String statement, String columns) {
        super(new StringBuilder(statement).append(" GROUP BY ").append(columns));
    }
    
    public HavingBuilder having(String function, Operator operator, String value) {
        return new HavingBuilder(statement, function, operator, value);
    }
    
}