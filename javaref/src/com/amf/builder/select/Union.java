package com.amf.builder.select;

public interface Union {
    
    default SelectBuilder union(boolean all, boolean distinct, String columns, String tables) {
        return new SelectBuilder(new StringBuilder(toString()), all, distinct, columns, tables);
    }    
    
}