package com.amf.builder.select;

import com.amf.builder.AbstractSQLBuilder;
import com.amf.builder.Operator;

public class HavingBuilder extends AbstractSQLBuilder implements OrderBy, Union {

    HavingBuilder(StringBuilder statement, String function, Operator operator, String value) {
        super(new StringBuilder(statement).append(" HAVING ").append(function).append(" ").append(operator).append(" ").append(value));
    }
    
}