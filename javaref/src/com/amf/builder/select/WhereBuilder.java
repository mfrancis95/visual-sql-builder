package com.amf.builder.select;

import com.amf.builder.AbstractSQLBuilder;
import com.amf.builder.Operator;

public class WhereBuilder extends AbstractSQLBuilder implements OrderBy, Union {

    WhereBuilder(String statement, String column, Operator operator, String value) {
        super(new StringBuilder(statement).append(" WHERE ").append(column).append(" ").append(operator).append(" ").append(value));
    }
    
    WhereBuilder(StringBuilder statement, boolean and, String column, Operator operator, String value) {
        super(new StringBuilder(statement).append(and ? " AND " : " OR ").append(column).append(" ").append(operator).append(" ").append(value));
    }
    
    public WhereBuilder and(String column, Operator operator, String value) {
        return new WhereBuilder(statement, true, column, operator, value);
    }
    
    public WhereBuilder or(String column, Operator operator, String value) {
        return new WhereBuilder(statement, false, column, operator, value);
    }

}