package com.amf.builder.select;

import com.amf.builder.Order;

public interface OrderBy {
    
    default OrderByBuilder orderBy(String column, Order order) {
        return new OrderByBuilder(toString(), column, order);
    }
    
}