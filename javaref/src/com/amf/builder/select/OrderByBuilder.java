package com.amf.builder.select;

import com.amf.builder.AbstractSQLBuilder;
import com.amf.builder.Order;

public class OrderByBuilder extends AbstractSQLBuilder implements OrderBy, Union {

    OrderByBuilder(String statement, String column, Order order) {
        super(new StringBuilder(statement).append(" ORDER BY ").append(column).append(" ").append(order));
    }
    
    OrderByBuilder(StringBuilder statement, String column, Order order) {
        super(new StringBuilder(statement).append(", ").append(column).append(" ").append(order));
    }
    
    public OrderByBuilder orderBy(String column, Order order) {
        return new OrderByBuilder(statement, column, order);
    }
    
}