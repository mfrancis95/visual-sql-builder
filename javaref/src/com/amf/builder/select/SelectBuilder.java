package com.amf.builder.select;

import com.amf.builder.AbstractSQLBuilder;

public class SelectBuilder extends AbstractSQLBuilder implements GroupBy, Join, OrderBy, Union, Where {

    public SelectBuilder(boolean distinct, String columns, String tables) {
        super(new StringBuilder("SELECT").append(distinct ? " DISTINCT " : " ").append(columns).append(" FROM ").append(tables));
    }
    
    SelectBuilder(StringBuilder statement, boolean all, boolean distinct, String columns, String tables) {
        super(new StringBuilder(statement).append(" UNION ").append(all ? "ALL SELECT" : "SELECT").append(distinct ? " DISTINCT " : " ").append(columns).append(" FROM ").append(tables));
    }
    
}