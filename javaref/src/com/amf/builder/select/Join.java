package com.amf.builder.select;

import com.amf.builder.JoinType;

public interface Join {
    
    default JoinBuilder join(JoinType joinType, String table, String column1, String column2) {
        return new JoinBuilder(toString(), joinType, table, column1, column2);
    }
    
}