package com.amf.builder.select;

import com.amf.builder.AbstractSQLBuilder;
import com.amf.builder.JoinType;

public class JoinBuilder extends AbstractSQLBuilder implements GroupBy, Join, Union, Where {

    JoinBuilder(String statement, JoinType join, String table, String column1, String column2) {
        super(new StringBuilder(statement).append(" ").append(join).append(" ").append(table).append(" ON ").append(column1).append(" = ").append(column2));
    }
    
    JoinBuilder(StringBuilder statement, boolean and, String column1, String column2) {
        super(new StringBuilder(statement).append(and ? " AND " : " OR ").append(column1).append(" = ").append(column2));
    }
    
    public JoinBuilder and(String column1, String column2) {
        return new JoinBuilder(statement, true, column1, column2);
    }
    
    public JoinBuilder or(String column1, String column2) {
        return new JoinBuilder(statement, false, column1, column2);
    }
    
}