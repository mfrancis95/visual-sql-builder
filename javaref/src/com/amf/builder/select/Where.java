package com.amf.builder.select;

import com.amf.builder.Operator;

public interface Where {
    
    default WhereBuilder where(String column, Operator operator, String value) {
        return new WhereBuilder(toString(), column, operator, value);
    }
    
}