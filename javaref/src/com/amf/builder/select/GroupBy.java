package com.amf.builder.select;

public interface GroupBy {
    
    default GroupByBuilder groupBy(String columns) {
        return new GroupByBuilder(toString(), columns);
    }
    
}