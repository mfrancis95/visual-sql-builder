package com.amf.test;

import static com.amf.builder.JoinType.*;
import static com.amf.builder.Operator.*;
import static com.amf.builder.Order.*;
import com.amf.builder.SQLBuilder;

public class Test {
    
    public static void main(String[] args) {
        String statement = SQLBuilder
                .select(true, "Orders.OrderID, Customers.CustomerName, Orders.OrderDate", "Orders")
                .join(INNER_JOIN, "Customers", "Orders.CustomerID", "Customers.CustomerID")
                .where("Country", EQUAL_TO, "'Mexico'")
                .or("Country", EQUAL_TO, "'Germany'")
                .orderBy("Country", DESCENDING)
                .toString();
        System.out.println(statement);
    }
    
}